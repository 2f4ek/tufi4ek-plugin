<?php

/*
Plugin Name: Tufi4ek Plugin
Plugin URI: https://bitbucket.org/2f4ek/tufi4ek-plugin
Description: Custom plugin for everything
Version: 1.0.0
Author: Alexandr "2f4ek" Tereshchenko
Author URI: https://bitbucket.org/2f4ek
License: GPLv2 or later
Text Domain: tufi4ek-plugin
 */

/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If the program does terminal interaction, make it output a short
notice like this when it starts in an interactive mode:

Copyright (C) 2018  Inc.
*/
defined('ABSPATH') or die('You lame go away!!!');

if(file_exists(dirname(__FILE__).'/vendor/autoload.php') ) {
    require_once dirname(__FILE__).'/vendor/autoload.php';
}

function activate_tufi4ek_plugin()
{
    \Inc\Base\Activate::activate();
}
register_activation_hook(__FILE__,  'activate_tufi4ek_plugin');

function deactivate_tufi4ek_plugin()
{
    \Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook(__FILE__, 'deactivate_tufi4ek_plugin');

if (class_exists(\Inc\Init::class)) {
    Inc\Init::register_services();
}
