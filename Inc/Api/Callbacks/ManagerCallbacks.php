<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class ManagerCallbacks extends BaseController
{

    public function checkboxSanitize($input)
    {
        $output = [];
        foreach ($this->managers as $key => $manager) {
            $output[$key] = (isset($input[$key]) ? true : false);
        }
        return $output;
    }

    public function adminSectionManager()
    {
        echo 'Manage the Sections and Features of plugin, just check what you want to see on your site.';
    }

    public function checkboxField($args)
    {
        $name = $args['label_for'];
        $class = $args['class'];
        $option_name = $args['option_name'];
        $checkbox = get_option($option_name);
        $checked = isset($checkbox[$name]) ? ($checkbox[$name] == true ? true : false) : false;

        echo '<div class="' . $class . '"><input type="checkbox" id="' . $name . '" name="' . $option_name . '[' . $name . ']" value="1" class="" ' . ($checked ? 'checked' : '') . '><label for="' . $name . '"><div></div></label></div>';
    }

}