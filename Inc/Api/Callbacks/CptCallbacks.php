<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc\Api\Callbacks;


class CptCallbacks
{

    public function cptSectionManager()
    {
        echo 'Create your Custom Post Types';
    }

    public function cptSanitize($input)
    {
        $output = get_option('tufi4ek_plugin_cpt');

        if (!get_option('tufi4ek_plugin_cpt')) {
            $output[$input['post_type']] = $input;
            return $output;
        }

        if (isset($_POST['remove'])) {
            unset($output[$_POST['remove']]);
            return $output;
        }

        foreach ($output as $key => $value) {
            if ($input['post_type'] === $key) {
                $output[$key] = $input;
            } else {
                $output[$input['post_type']] = $input;
            }
        }

        return $output;
    }

    public function textField($args)
    {
        $name = $args['label_for'];
        $option_name = $args['option_name'];
        $value = '';
        $type = '';
        if ( isset($_POST['edit_post_type']) ) {
            $input = get_option($option_name);
            $value = $input[$_POST['edit_post_type']][$name]?: '';
            $type = ($name ==  'post_type') ? 'readonly' : '';
        }

        echo '<input type="text" class="regular-text" id="' . $name . '" name="' . $option_name . '[' . $name . ']" value="' . $value . '" placeholder="' . $args['placeholder'] . '" required '.$type.'>';
    }

    public function checkboxField($args)
    {
        $name = $args['label_for'];
        $classes = $args['class'];
        $option_name = $args['option_name'];
        if (isset($_POST['edit_post_type'])) {
            $checkbox = get_option($option_name);
            $checked =  (isset($checkbox[$_POST['edit_post_type']][$name]) ) ?: false;
        }

        echo '<div class="' . $classes . '"><input type="checkbox" id="' . $name . '" name="' . $option_name . '[' . $name . ']" value="1" class="" '.($checked ? 'checked' : '').'><label for="' . $name . '"><div></div></label></div>';
    }
}