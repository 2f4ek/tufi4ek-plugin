<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc\Api\Callbacks;


class TaxonomyCallbacks
{
    public function SectionManager()
    {
        echo 'Create your Custom Post Types';
    }

    public function Sanitize($input)
    {
        $output = get_option('tufi4ek_plugin_tax');

        if (!get_option('tufi4ek_plugin_tax')) {
            $output[$input['taxonomy']] = $input;
            return $output;
        }

        if (isset($_POST['remove'])) {
            unset($output[$_POST['remove']]);
            return $output;
        }

        foreach ($output as $key => $value) {
            if ($input['taxonomy'] === $key) {
                $output[$key] = $input;
            } else {
                $output[$input['taxonomy']] = $input;
            }
        }

        return $output;
    }

    public function textField($args)
    {
        $name = $args['label_for'];
        $option_name = $args['option_name'];
        $value = '';
        $type = '';
        if ( isset($_POST['edit_taxonomy']) ) {
            $input = get_option($option_name);
            $value = $input[$_POST['edit_taxonomy']][$name]?: '';
            $type = ($name ==  'taxonomy') ? 'readonly' : '';
        }

        echo '<input type="text" class="regular-text" id="' . $name . '" name="' . $option_name . '[' . $name . ']" value="' . $value . '" placeholder="' . $args['placeholder'] . '" required '.$type.'>';
    }

    public function checkboxField($args)
    {
        $name = $args['label_for'];
        $classes = $args['class'];
        $option_name = $args['option_name'];
        $checked = false;
        if (isset($_POST['edit_taxonomy'])) {
            $checkbox = get_option($option_name);
            $checked =  (isset($checkbox[$_POST['edit_taxonomy']][$name]) ) ?: false;
        }

        echo '<div class="' . $classes . '"><input type="checkbox" id="' . $name . '" name="' . $option_name . '[' . $name . ']" value="1" class="" '.($checked ? 'checked' : '').'><label for="' . $name . '"><div></div></label></div>';
    }

    public function checkboxMultiFields($args)
    {
        $output = '';
        $name = $args['label_for'];
        $classes = $args['class'];
        $option_name = $args['option_name'];
        $checkbox = isset($_POST['edit_taxonomy']) ? get_option($option_name) : false;

        $post_types = get_post_types( ['show_ui' => true ] );
        foreach ($post_types as $post) {
            $checked = isset($_POST['edit_taxonomy']) ? ( (isset($checkbox[$_POST['edit_taxonomy']][$name][$post]) ) ?: false) : false;

            $output .= '<div class="' . $classes . ' mb-10"><input type="checkbox" id="' . $post . '" name="' . $option_name . '[' . $name . ']['.$post.']" value="1" class="" '.($checked ? 'checked' : '').'><label for="' . $post . '"><div></div></label><strong>'.$post.'</strong></div>';
        }

        echo $output;

    }



}