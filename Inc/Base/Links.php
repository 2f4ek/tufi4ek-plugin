<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;

class Links extends BaseController
{

    /**
     * register plugin edit link
     */
    public function register()
    {
        add_filter("plugin_action_links_$this->plugin_name", [$this, 'settings_link']);
    }

    /**
     * @param $links
     * @return array of links
     */
    public function settings_link($links)
        {
            $settings_link = '<a href="admin.php?page=tufi4ek_plugin">Settings</a>';
            $links[] = $settings_link;
            return $links;
        }

}