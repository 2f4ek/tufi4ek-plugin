<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;

class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();
        $default = [];

        if (!get_option('tufi4ek_plugin'))
            update_option( 'tufi4ek_plugin', $default );

        if (!get_option('tufi4ek_plugin_cpt'))
            update_option( 'tufi4ek_plugin_cpt', $default );

        if (!get_option('tufi4ek_plugin_tax'))
            update_option( 'tufi4ek_plugin_tax', $default );

    }

}