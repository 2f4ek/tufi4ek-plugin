<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;


class Enqueue extends BaseController
{
    public function register()
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueue']);
    }

    /**
     * Enqueue plugin Scripts n Styles
     */
    public function enqueue()
    {
        wp_enqueue_script('media-upload');
        wp_enqueue_media();
        wp_enqueue_style('tufi4ekstyles', $this->plugin_url."assets/style.css");
        wp_enqueue_script('tufi4ekscript', $this->plugin_url."assets/myscript.js");
    }
}