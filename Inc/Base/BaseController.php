<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;

class BaseController
{
    public $plugin_path;

    public $plugin_url;

    public $plugin_name;

    public $managers = [];

    public function __construct()
    {
        $this->plugin_path = plugin_dir_path(dirname(__FILE__,2));
        $this->plugin_url = plugin_dir_url(dirname(__FILE__,2));
        $this->plugin_name = plugin_basename(dirname(__FILE__,3)).'/tufi4ek-plugin.php';
        $this->managers = [
            'cpt_manager' => 'Activate CPT manager',
            'taxonomy_manager' => 'Activate Taxonomies Manager',
            'media_widget' => 'Activate Media Widget',
            'gallery_manager' => 'Activate Gallery Manager',
            'testimonial_manager' => 'Activate Testimonials Manager',
            'templates_manager' => 'Activate Templates Manager',
            'login_manager' => 'Activate Login Manager',
            'membership_manager' => 'Activate Membership Manager',
            'chat_manager' => 'Activate Chat Manager'
        ];
    }
    public function activated( string $option_name )
    {
        $option = get_option('tufi4ek_plugin');
        return isset( $option[$option_name] ) ? $option[$option_name] : false;
    }
}
