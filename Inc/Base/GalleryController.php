<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;

use Inc\Api\SettingsApi;
use Inc\Api\Callbacks\AdminCallbacks;


class GalleryController extends BaseController
{
    public $callbacks;
    public $subpages = [];
    public $settings;

    public function register()
    {

        if ( ! $this->activated('gallery_manager') ) return;

        $this->settings = new SettingsApi();
        $this->callbacks = new AdminCallbacks();
        $this->setSubPages();
        $this->settings->addSubPages($this->subpages)->register();
    }

    public function setSubPages()
    {
        $this->subpages = [
            [
                'parent_slug' => 'tufi4ek_plugin',
                'page_title' => 'Gallery Manager',
                'menu_title' => 'Gallery Manager',
                'capability' => 'manage_options',
                'menu_slug' => 'tufi4ek_gallery',
                'callback' => [$this->callbacks, 'adminGallery']
            ]
        ];
    }

}