<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;

use Inc\Api\SettingsApi;
use Inc\Api\Callbacks\AdminCallbacks;


class ChatController extends BaseController
{
    public $callbacks;
    public $subpages = [];
    public $settings;

    public function register()
    {
        if ( ! $this->activated('chat_manager') ) return;

        $this->settings = new SettingsApi();
        $this->callbacks = new AdminCallbacks();
        $this->setSubPages();
        $this->settings->addSubPages($this->subpages)->register();
    }

    public function setSubPages()
    {
        $this->subpages = [
            [
                'parent_slug' => 'tufi4ek_plugin',
                'page_title' => 'Chat Manager',
                'menu_title' => 'Chat Manager',
                'capability' => 'manage_options',
                'menu_slug' => 'tufi4ek_chat',
                'callback' => [$this->callbacks, 'adminChat']
            ]
        ];
    }

}