<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc\Base;

use Inc\Api\SettingsApi;
use Inc\Api\Callbacks\AdminCallbacks;
use Inc\Api\Callbacks\TaxonomyCallbacks;


class TaxonomyController extends BaseController
{
    public $settings;
    public $callbacks;
    public $tax_callbacks;
    public $subpages = [];
    public $taxonomies = [];

    public function register()
    {
        if (!$this->activated('taxonomy_manager')) return;

        $this->settings = new SettingsApi();
        $this->callbacks = new AdminCallbacks();
        $this->tax_callbacks = new TaxonomyCallbacks();
        $this->setSubPages();
        $this->setSettings();
        $this->setSections();
        $this->setFields();
        $this->settings->addSubPages($this->subpages)->register();

        $this->storeCustomTaxonomies();

        if (!empty($this->taxonomies))
            add_action('init', [$this, 'registerTaxonomy']);
    }

    public function setSubPages()
    {
        $this->subpages = [
            [
                'parent_slug' => 'tufi4ek_plugin',
                'page_title' => 'Taxonomies Manager',
                'menu_title' => 'Taxonomies Manager',
                'capability' => 'manage_options',
                'menu_slug' => 'tufi4ek_taxonomy',
                'callback' => [$this->callbacks, 'adminTaxonomy']
            ]
        ];
    }

    public function setSettings()
    {
        $args = [
            [
                'option_group' => 'tufi4ek_plugin_tax_settings',
                'option_name' => 'tufi4ek_plugin_tax',
                'callback' => [$this->tax_callbacks, 'Sanitize']
            ]
        ];

        $this->settings->setSettings($args);
    }

    public function setSections()
    {
        $args = [
            [
                'id' => 'tufi4ek_tax_index',
                'title' => 'Custom Taxonomy Manager',
                'callback' => [$this->tax_callbacks, 'SectionManager'],
                'page' => 'tufi4ek_taxonomy'
            ]
        ];
        $this->settings->setSections($args);
    }

    public function setFields()
    {
        $args = [
            [
                'id' => 'taxonomy',
                'title' => 'Custom Taxonomy ID',
                'callback' => [
                    $this->tax_callbacks, 'textField'
                ],
                'page' => 'tufi4ek_taxonomy',
                'section' => 'tufi4ek_tax_index',
                'args' => [
                    'option_name' => 'tufi4ek_plugin_tax',
                    'label_for' => 'taxonomy',
                    'placeholder' => 'eg. genre',
                    'array' => 'taxonomy'
                ]
            ],
            [
                'id' => 'singular_name',
                'title' => 'Singular Name',
                'callback' => [
                    $this->tax_callbacks, 'textField'
                ],
                'page' => 'tufi4ek_taxonomy',
                'section' => 'tufi4ek_tax_index',
                'args' => [
                    'option_name' => 'tufi4ek_plugin_tax',
                    'label_for' => 'singular_name',
                    'placeholder' => 'eg. Genre',
                    'array' => 'taxonomy'
                ]
            ],
            [
                'id' => 'hierarchical',
                'title' => 'Hierarchical',
                'callback' => [
                    $this->tax_callbacks, 'checkboxField'
                ],
                'page' => 'tufi4ek_taxonomy',
                'section' => 'tufi4ek_tax_index',
                'args' => [
                    'option_name' => 'tufi4ek_plugin_tax',
                    'label_for' => 'hierarchical',
                    'class' => 'ui-toggle',
                    'array' => 'taxonomy'
                ]
            ],
            [
                'id' => 'objects',
                'title' => 'Post Types',
                'callback' => [
                    $this->tax_callbacks, 'checkboxMultiFields'
                ],
                'page' => 'tufi4ek_taxonomy',
                'section' => 'tufi4ek_tax_index',
                'args' => [
                    'option_name' => 'tufi4ek_plugin_tax',
                    'label_for' => 'objects',
                    'class' => 'ui-toggle',
                    'array' => 'taxonomy'
                ]
            ]
        ];
        $this->settings->setFields($args);
    }

    public function storeCustomTaxonomies()
    {
        // get the tax array
        $options = (get_option('tufi4ek_plugin_tax')) ?: [];

        // store those info into array
        foreach ($options as $option) {
            $labels = [
                'name'                       => $option['taxonomy'],
                'singular_name'              => $option['singular_name'],
                'search_items'               => $option['singular_name'],
                'menu_name'                  => $option['singular_name'],
                'all_items'                  => 'All Items'.$option['singular_name'],
                'parent_item'                => $option['singular_name'].' Item',
                'parent_item_colon'          => $option['singular_name'].' Item:',
                'new_item_name'              => 'New '.$option['singular_name'].' Name',
                'add_new_item'               =>  'Add New '.$option['singular_name'],
                'edit_item'                  =>  'Edit '.$option['singular_name'],
                'update_item'                =>  'Update '.$option['singular_name'],
                'view_item'                  =>  'View '.$option['singular_name'],
                'separate_items_with_commas' =>  'Separate '.$option['singular_name'].' with commas',
                'add_or_remove_items'        =>  'Add or remove '.$option['singular_name'],
                'choose_from_most_used'      =>  'Choose from the most used',
                'popular_items'              =>  'Popular '.$option['singular_name'],
                'not_found'                  =>  'Not Found',
                'no_terms'                   =>  'No '.$option['singular_name'],
                'items_list'                 =>  $option['singular_name'].' list',
                'items_list_navigation'      =>  $option['singular_name'].' list navigation',
            ];
            $this->taxonomies[] = [
                'labels'                     => $labels,
                'hierarchical'               => isset($option['hierarchical']) ? true : false,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'rewrite'                     => ['slug' => $option['taxonomy']],
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'objects'                    => isset($option['objects']) ? $option['objects'] : null,
            ];
        }
    }

    public function registerTaxonomy()
    {
        foreach ($this->taxonomies as $taxonomy) {
            $objects = isset($taxonomy['objects']) ? array_keys($taxonomy['objects']) : null;
            register_taxonomy( $taxonomy['rewrite']['slug'], $objects, $taxonomy );
        }
    }

}