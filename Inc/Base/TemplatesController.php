<?php
/**
 * @package Tufi4ekPlugin
 */
namespace Inc\Base;

use Inc\Api\SettingsApi;
use Inc\Api\Callbacks\AdminCallbacks;


class TemplatesController extends BaseController
{
    public $callbacks;
    public $subpages = [];
    public $settings;

    public function register()
    {
        if ( ! $this->activated('templates_manager') ) return;

        $this->settings = new SettingsApi();
        $this->callbacks = new AdminCallbacks();
        $this->setSubPages();
        $this->settings->addSubPages($this->subpages)->register();
    }

    public function setSubPages()
    {
        $this->subpages = [
            [
                'parent_slug' => 'tufi4ek_plugin',
                'page_title' => 'Templates Manager',
                'menu_title' => 'Templates Manager',
                'capability' => 'manage_options',
                'menu_slug' => 'tufi4ek_templates',
                'callback' => [$this->callbacks, 'adminTemplates']
            ]
        ];
    }

}