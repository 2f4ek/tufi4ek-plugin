<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc\Base;

use Inc\Api\Widgets\MediaWidget;

class MediaWidgetController extends BaseController
{
    public function register()
    {
        if ( ! $this->activated('media_widget') ) return;

        $media_widget = new MediaWidget();

        $media_widget->register();

    }

}