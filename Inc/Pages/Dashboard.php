<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc\Pages;

use Inc\Api\Callbacks\ManagerCallbacks;
use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;


class Dashboard extends BaseController
{
    public $settings;

    public $callbacks;

    public $callbacks_mngr;

    public $pages = [];

    //public $subpages = [];

    public function register()
    {

        $this->settings = new SettingsApi();

        $this->callbacks = new AdminCallbacks();

        $this->callbacks_mngr = new ManagerCallbacks();

        $this->setPages();

        //$this->setSubPages();

        $this->setSettings();

        $this->setSections();

        $this->setFields();

        $this->settings->addPages($this->pages)->withSubPage('Dashboard')->register();
    }

    public function setPages()
    {
        $this->pages = [
            [
                'page_title' => 'Tufi4ek Plugin',
                'menu_title' => 'Tufi4ek',
                'capability' => 'manage_options',
                'menu_slug' => 'tufi4ek_plugin',
                'callback' => [$this->callbacks, 'adminDashboard'],
                'icon_url' => 'dashicons-store',
                'position' => 110
            ]
        ];
    }

    public function setSettings()
    {
        $args = [
            [
                'option_group' => 'tufi4ek_plugin_settings',
                'option_name' => 'tufi4ek_plugin',
                'callback' => [
                    $this->callbacks_mngr, 'checkboxSanitize'
                ]
            ]
        ];
        $this->settings->setSettings($args);
    }

    public function setSections()
    {
        $args = [
            [
                'id' => 'tufi4ek_admin_index',
                'title' => 'Settings Manager',
                'callback' => [
                    $this->callbacks_mngr, 'adminSectionManager'
                ],
                'page' => 'tufi4ek_plugin'
            ]
        ];

        $this->settings->setSections($args);
    }

    public function setFields()
    {
        $args = [];
        foreach ($this->managers as $manager => $title) {
            $args[] = [
                'id' => $manager,
                'title' => $title,
                'callback' => [
                    $this->callbacks_mngr, 'checkboxField'
                ],
                'page' => 'tufi4ek_plugin',
                'section' => 'tufi4ek_admin_index',
                'args' => [
                    'option_name' => 'tufi4ek_plugin',
                    'label_for' => $manager,
                    'class' => 'ui-toggle'
                ]
            ];
        }

        $this->settings->setFields($args);
    }
}