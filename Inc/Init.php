<?php
/**
 * @package Tufi4ekPlugin
 */

namespace Inc;

final class Init
{

    /**
     * @return array List Of Classes
     */
    public static function get_services()
    {
        return [
            Base\Enqueue::class,
            Pages\Dashboard::class,
            Base\Links::class,
            Base\CustomPostTypeController::class,
            Base\TaxonomyController::class,
            Base\MediaWidgetController::class,
            Base\GalleryController::class,
            Base\TestimonialController::class,
            Base\TemplatesController::class,
            Base\LoginController::class,
            Base\MembershipController::class,
            Base\ChatController::class,
        ];
    }

    /**
     * Initialize all Classes from get_services()
     * init register() of all Classes
     */
    public static function register_services()
    {
        foreach (self::get_services() as $class)
        {
            if (method_exists(self::instantiate($class), 'register'))
            {
                self::instantiate($class)->register();
            }
        }
    }

    /**
     * @param $class
     * @return mixed $class instance
     */
    private static function instantiate($class)
    {
        return new $class();
    }
}

