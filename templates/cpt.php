<div class="wrap">
    <h1>Custom Post Type manager</h1>
    <?php settings_errors(); ?>

    <ul class="nav nav-tabs">
        <li class="<?= (!isset($_POST['edit_post_type'])) ? 'active' : '' ?>"><a href="#tab-1">Your Custom Post
                Types</a></li>
        <li class="<?= (isset($_POST['edit_post_type'])) ? 'active' : '' ?>">
            <a href="#tab-2">
                <?= (isset($_POST['edit_post_type'])) ? 'Edit' : 'Add' ?> Custom Post Type
            </a>
        </li>
        <li><a href="#tab-3">Export</a></li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane <?= (!isset($_POST['edit_post_type'])) ? 'active' : '' ?>">
            <h3>Custom Post Types</h3>
            <?php $options = (get_option('tufi4ek_plugin_cpt')) ?: []; ?>
            <table class="cpt-table">
                <thead>
                <tr>
                    <th>Post Type</th>
                    <th>Singular Name</th>
                    <th>Plural Name</th>
                    <th>Public</th>
                    <th>Archive</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($options as $option) : ?>
                    <tr>
                        <td><?= $option['post_type'] ?></td>
                        <td><?= $option['singular_name'] ?></td>
                        <td><?= $option['plural_name'] ?></td>
                        <td class='text-center'>
                            <?= ($option['public']) ? "<span class='green dashicons dashicons-yes'></span>" : "<span class='red dashicons dashicons-no'></span>"; ?>
                        </td>
                        <td class='text-center'>
                            <?= ($option['has_archive']) ? "<span class='green dashicons dashicons-yes'></span>" : "<span class='red dashicons dashicons-no'></span>"; ?>
                        </td>
                        <td class='text-center'>
                            <form method="POST" action="" class="inline-block">
                                <input type="hidden" name="edit_post_type" value="<?= $option['post_type'] ?>">
                                <?php
                                submit_button('Edit', 'primary small', 'submit', false);
                                ?>
                            </form>
                            <form method="POST" action="options.php" class="inline-block">
                                <?php settings_fields('tufi4ek_plugin_cpt_settings');
                                submit_button('Delete', 'delete small', 'submit', false, [
                                    'onclick' => 'return confirm("Are you sure you want to delete ' . $option['post_type'] . ' Post Type? Data associated with it will not be deleted");'
                                ]);
                                ?>
                                <input type="hidden" name="remove" value="<?= $option['post_type'] ?>">
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
        <div id="tab-2" class="tab-pane <?= (isset($_POST['edit_post_type'])) ? 'active' : '' ?>">
            <form method="POST" action="options.php">
                <?php
                settings_fields('tufi4ek_plugin_cpt_settings');
                do_settings_sections('tufi4ek_cpt');
                submit_button();
                ?>
            </form>
        </div>
        <div id="tab-3" class="tab-pane">
            <h3>Export Your Custom Post Types</h3>
            <?php foreach ($options as $option) { ?>
                <h3><?= $option['singular_name'] ?></h3>
                <pre class="prettyprint">
function custom_post_type_<?=$option['post_type']?>() {

	$labels = array(
		'name'                  => _x( '<?= $option['post_type'] ?>', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( '<?= $option['singular_name'] ?>', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( '<?= $option['plural_name'] ?>', 'text_domain' ),
		'plural_name'             => __( '<?= $option['plural_name'] ?>', 'plural_name' ),
		'name_admin_bar'        => __( '<?= $option['plural_name'] ?>', 'text_domain' ),
		'archives'              => __( '<?= $option['plural_name'] ?> Archives', 'text_domain' ),
		'attributes'            => __( '<?= $option['plural_name'] ?> Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent <?= $option['singular_name'] ?>:', 'text_domain' ),
		'all_items'             => __( 'All <?= $option['plural_name'] ?>', 'text_domain' ),
		'add_new_item'          => __( 'Add New <?= $option['singular_name'] ?>', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New <?= $option['singular_name'] ?>', 'text_domain' ),
		'edit_item'             => __( 'Edit <?= $option['singular_name'] ?>', 'text_domain' ),
		'update_item'           => __( 'Update <?= $option['singular_name'] ?>', 'text_domain' ),
		'view_item'             => __( 'View <?= $option['singular_name'] ?>', 'text_domain' ),
		'view_items'            => __( 'View <?= $option['plural_name'] ?>', 'text_domain' ),
		'search_items'          => __( 'Search <?= $option['singular_name'] ?>', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into <?= $option['singular_name'] ?>', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this <?= $option['plural_name'] ?>', 'text_domain' ),
		'items_list'            => __( '<?= $option['plural_name'] ?> list', 'text_domain' ),
		'items_list_navigation' => __( '<?= $option['plural_name'] ?> list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter <?= $option['plural_name'] ?> list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( '<?= $option['plural_name'] ?>', 'text_domain' ),
		'description'           => __( '<?= $option['plural_name'] ?> Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => false,
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => <?= isset($option['public']) ? 'true' : 'false' ?>,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => <?= isset($option['has_archive']) ? 'true' : 'false'?>,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'post_type', $args );

}
add_action( 'init', 'custom_post_type_<?=$option['post_type']?>', 0 );
            </pre>
            <?php } ?>
        </div>
    </div>


</div>

