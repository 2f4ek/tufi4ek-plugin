<div class="wrap">
    <h1>Taxonomy Manager</h1>
    <?php settings_errors(); ?>

    <ul class="nav nav-tabs">
        <li class="<?= (!isset($_POST['edit_taxonomy'])) ? 'active' : '' ?>"><a href="#tab-1">Your Taxonomies</a></li>
        <li class="<?= (isset($_POST['edit_taxonomy'])) ? 'active' : '' ?>">
            <a href="#tab-2">
                <?= (isset($_POST['edit_taxonomy'])) ? 'Edit' : 'Add' ?> Custom Taxonomies
            </a>
        </li>
        <li><a href="#tab-3">Export</a></li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane <?= (!isset($_POST['edit_taxonomy'])) ? 'active' : '' ?>">
            <h3>Custom Taxonomies</h3>
            <?php $options = (get_option('tufi4ek_plugin_tax')) ?: []; ?>
            <table class="cpt-table">
                <thead>
                <tr>
                    <th>Post Type</th>
                    <th>Singular Name</th>
                    <th>Hierarchical</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($options as $option) : ?>
                    <tr>
                        <td><?= $option['taxonomy'] ?></td>
                        <td><?= $option['singular_name'] ?></td>
                        <td class='text-center'>
                            <?= ($option['hierarchical']) ? "<span class='green dashicons dashicons-yes'></span>" : "<span class='red dashicons dashicons-no'></span>"; ?>
                        </td>
                        <td class='text-center'>
                            <form method="POST" action="" class="inline-block">
                                <input type="hidden" name="edit_taxonomy" value="<?= $option['taxonomy'] ?>">
                                <?php
                                submit_button('Edit', 'primary small', 'submit', false);
                                ?>
                            </form>
                            <form method="POST" action="options.php" class="inline-block">
                                <?php settings_fields('tufi4ek_plugin_tax_settings');
                                submit_button('Delete', 'delete small', 'submit', false, [
                                    'onclick' => 'return confirm("Are you sure you want to delete ' . $option['taxonomy'] . ' Taxonomy? Data associated with it will not be deleted");'
                                ]);
                                ?>
                                <input type="hidden" name="remove" value="<?= $option['taxonomy'] ?>">
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
        <div id="tab-2" class="tab-pane <?= (isset($_POST['edit_taxonomy'])) ? 'active' : '' ?>">
            <form method="POST" action="options.php">
                <?php
                settings_fields('tufi4ek_plugin_tax_settings');
                do_settings_sections('tufi4ek_taxonomy');
                submit_button();
                ?>
            </form>
        </div>
        <div id="tab-3" class="tab-pane">
            <h3>Export Your Custom Taxonomies Types</h3>
        </div>
    </div>


</div>

